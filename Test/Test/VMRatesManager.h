//
//  VMRatesManager.h
//  Test
//
//  Created by Azamat Valitov on 07.05.15.
//  Copyright (c) 2015 VoltMobi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMRate.h"
@interface VMRatesManager : NSObject
+(VMRatesManager *)sharedInstance;
-(void)getRate : (NSString *)from to : (NSString *)to completition : (void (^)(VMRate *rate, NSError *error))completition;
@end
