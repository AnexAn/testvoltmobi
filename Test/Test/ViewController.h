//
//  ViewController.h
//  Test
//
//  Created by Azamat Valitov on 07.05.15.
//  Copyright (c) 2015 VoltMobi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *rate;
@property (weak, nonatomic) IBOutlet UILabel *fromTo;
@property (weak, nonatomic) IBOutlet UILabel *summary;
@property (weak, nonatomic) IBOutlet UILabel *date;
- (IBAction)menuAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *blockView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;

@property (weak, nonatomic) IBOutlet UIView *menuView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menuBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menuHeightConstraint;
- (IBAction)closeMenuAction:(id)sender;
- (IBAction)rateAction:(id)sender;
- (IBAction)touchDown:(id)sender;
- (IBAction)touchCancelled:(id)sender;
- (IBAction)touchUpOutSide:(id)sender;
@end

