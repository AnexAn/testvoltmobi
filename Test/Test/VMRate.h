//
//  VMRate.h
//  Test
//
//  Created by Azamat Valitov on 07.05.15.
//  Copyright (c) 2015 VoltMobi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMRate : NSObject
@property (nonatomic, strong) NSString *from;
@property (nonatomic, strong) NSString *to;
@property (nonatomic, strong) NSString *rate;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, readwrite) float percent;
-(id)initWithDictionary : (NSDictionary *)dictionary;
@end
