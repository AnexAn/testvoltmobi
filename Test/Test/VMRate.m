//
//  VMRate.m
//  Test
//
//  Created by Azamat Valitov on 07.05.15.
//  Copyright (c) 2015 VoltMobi. All rights reserved.
//

#import "VMRate.h"
@interface VMRate(){
    
}
@end
@implementation VMRate
-(id)initWithDictionary : (NSDictionary *)dictionary{
    self = [super init];
    
    if (self) {
        self.from = dictionary[@"from"];
        self.to = dictionary[@"to"];
        self.rate = [NSString stringWithFormat:@"%.3f", [dictionary[@"rate"] floatValue]];
        NSDate *now = [NSDate date];
        NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
        [timeFormat setDateFormat:@"HH:mm"];
        self.time = [timeFormat stringFromDate:now];
    }
    
    return self;
}
@end
