//
//  ViewController.m
//  Test
//
//  Created by Azamat Valitov on 07.05.15.
//  Copyright (c) 2015 VoltMobi. All rights reserved.
//

#import "ViewController.h"
#import "VMRatesManager.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.blockView.alpha = 0.f;
    [self.activity stopAnimating];
    
    [self update:1];
}
-(void)update : (NSInteger)index{
    
    NSArray *rateConsts = @[@[@"USD",@"RUB"],@[@"USD",@"EUR"],@[@"EUR",@"RUB"],@[@"EUR",@"USD"],@[@"RUB",@"USD"],@[@"RUB",@"EUR"]];
    
    NSArray *neededRateConsts = [rateConsts objectAtIndex:index - 1];
    
    
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [self.activity startAnimating];
    [UIView animateWithDuration:0.3f delay:0.f usingSpringWithDamping:1.f initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.blockView.alpha = 1;
    } completion:^(BOOL finished){
    }];
    [[VMRatesManager sharedInstance] getRate : [neededRateConsts firstObject] to:[neededRateConsts lastObject] completition:^(VMRate *rate, NSError *error){
        
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [self.activity stopAnimating];
        [UIView animateWithDuration:0.3f delay:0.f usingSpringWithDamping:1.f initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.blockView.alpha = 0;
        } completion:^(BOOL finished){
        }];
        
        if (rate) {
            self.fromTo.text = [NSString stringWithFormat:@"%@ → %@",rate.from, rate.to];
            self.rate.text = rate.rate;
            self.date.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"UPDATEDAT", @""),rate.time];
            if (rate.percent < 0) {
                self.summary.text = [NSString stringWithFormat:@"%@ %@ %@ %.0f %@",NSLocalizedString(@"SINCE", @""), rate.from,NSLocalizedString(@"FELL", @""), fabs(rate.percent), NSLocalizedString(@"PERCENT", @"")];
                self.summary.textColor = [UIColor colorWithRed:208.f/255.f green:2.f/255.f blue:27.f/255.f alpha:1.f];
            }else{
                self.summary.text = [NSString stringWithFormat:@"%@ %@ %@ %.0f %@",NSLocalizedString(@"SINCE", @""), rate.from,NSLocalizedString(@"ROSE", @""), fabs(rate.percent), NSLocalizedString(@"PERCENT", @"")];
                self.summary.textColor = [UIColor colorWithRed:126.f/255.f green:211.f/255.f blue:33.f/255.f alpha:1.f];
            }
        }else if(error){
            NSLog(@"%@", error);
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)menuAction:(id)sender {
    if (self.menuBottomConstraint.constant != 0) {
        self.menuBottomConstraint.constant = 0;
        NSLog(@"opening nemu");
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        [UIView animateWithDuration:0.3f delay:0.f usingSpringWithDamping:1.f initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished){
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        }];
    }
}
- (IBAction)closeMenuAction:(id)sender {
    [self closeMenu];
}
-(void)closeMenu{
    if (self.menuBottomConstraint.constant == 0) {
        NSLog(@"closing nemu");
        self.menuBottomConstraint.constant = -self.menuHeightConstraint.constant;
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        [UIView animateWithDuration:0.3f delay:0.f usingSpringWithDamping:1.f initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished){
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        }];
    }
}
- (IBAction)rateAction:(UIButton *)sender {
    [sender.titleLabel setFont:[UIFont fontWithName:@"Lato" size:14.f]];
    
    [self update:sender.tag];
    
    [self closeMenu];
}

- (IBAction)touchDown:(UIButton *)sender {
    [sender.titleLabel setFont:[UIFont fontWithName:@"Lato-Black" size:14.f]];
}

- (IBAction)touchCancelled:(UIButton *)sender {
    [sender.titleLabel setFont:[UIFont fontWithName:@"Lato" size:14.f]];
}

- (IBAction)touchUpOutSide:(UIButton *)sender {
    [sender.titleLabel setFont:[UIFont fontWithName:@"Lato" size:14.f]];
}
@end
