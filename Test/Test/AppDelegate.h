//
//  AppDelegate.h
//  Test
//
//  Created by Azamat Valitov on 07.05.15.
//  Copyright (c) 2015 VoltMobi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

