//
//  VMRatesManager.m
//  Test
//
//  Created by Azamat Valitov on 07.05.15.
//  Copyright (c) 2015 VoltMobi. All rights reserved.
//

#import "VMRatesManager.h"
#import "ASIHTTPRequest.h"

#define kJrKey @"jr-55589a56e026bbe8081ae4b7fba42d42"

@implementation VMRatesManager
+(VMRatesManager *)sharedInstance{
    static dispatch_once_t pred;
    static VMRatesManager *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}
-(void)getYesterdayRate : (NSString *)from to : (NSString *)to completition : (void (^)(NSString *yesterdayRate, NSError *error))completition{
    NSDate *today = [NSDate date];
    NSDate *yesterday = [today dateByAddingTimeInterval: -86400.0];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    NSString *searchString = [NSString stringWithFormat:@"http://jsonrates.com/historical/?from=%@&to=%@&date=%@&apiKey=%@",from, to,[dateFormat stringFromDate:yesterday], kJrKey];
    NSURL *url = [NSURL URLWithString:searchString];
    __block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    
    [request setCompletionBlock:^{
        NSString *responseString = [request responseString];
        NSLog(@"responseString : %@",responseString);
        
        NSData *responseData = [request responseData];
        
        NSError* error = nil;
        NSDictionary *parsedResponce = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        if (error) {
            completition(0, error);
        }else{
            NSDictionary *rates = parsedResponce[@"rates"];
            NSDictionary *yesterdayRate = [[rates allValues] firstObject];
            NSString *rate = [NSString stringWithFormat:@"%.3f", [yesterdayRate[@"rate"] floatValue]];
            completition(rate, nil);
        }
        
        request = nil;
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        completition(0, error);
        request = nil;
    }];
    [request startAsynchronous];
}
-(void)getRate : (NSString *)from to : (NSString *)to completition : (void (^)(VMRate *rate, NSError *error))completition{
    
    [self getYesterdayRate:from to:to completition:^(NSString *yesterdayRate, NSError *error){
        if (error == nil) {
            NSString *searchString = [NSString stringWithFormat:@"http://jsonrates.com/get/?from=%@&to=%@&apiKey=%@",from,to,kJrKey];
            NSURL *url = [NSURL URLWithString:searchString];
            __block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
            
            [request setCompletionBlock:^{
                NSString *responseString = [request responseString];
                NSLog(@"responseString : %@",responseString);
                
                NSData *responseData = [request responseData];
                
                NSError* error = nil;
                NSDictionary *parsedResponce = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
                if (error) {
                    completition(nil, error);
                }else{
                    VMRate *rate = [[VMRate alloc] initWithDictionary:parsedResponce];
                    
                    float todayRate = [rate.rate floatValue];
                    float oldRate = [yesterdayRate floatValue];
                    
                    float percent = ((todayRate - oldRate) / oldRate) * 100.f;
                    rate.percent = percent;
                    
                    completition(rate, nil);
                }
                
                request = nil;
            }];
            [request setFailedBlock:^{
                NSError *error = [request error];
                completition(nil, error);
                request = nil;
            }];
            [request startAsynchronous];
        }else{
            completition(nil, error);
        }
    }];
}
@end
